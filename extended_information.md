<h2>About</h2>
<p>Starnix is a community effort lead by FOSS enthusiasts for the purpose of establishing ActivityPub Software and promoting Fediverse usage.  The primary topics for this Mastodon instance include but are not limited to software technology, including FOSS, Unix and Unix-like operating systems, and gaming.</p>

<h2><strong>Fediverse Friendly Moderation Covenant</strong></h2>

<strong>This instance commits to the following</strong>

<h3>1. Administration and Moderation will be handled as neutral as possible</h3>
<p>Reports and widespread information on specific instances (often known as <strong>bad actors</strong>) will be verified
and actions might be taken as necessary.</p>

<h3>2. Actions taken will respond to the bare minimum to keep the network operating and not influence the ability of the instances users to connect to other people</h3>
<ul>
<li>If another instance spreads media content of questionable legality for the region the current instance
  is operating from, these <strong>files might be rejected</strong> from being saved.</li>
<li>If another instance is sharing hateful messages that impact local users, there might be consideration
  of <strong>silencing</strong> that specific instance from being visible in timelines. If users follow and/or wish to follow
  users on the target instance, they are still free to do so.</li>
<li>If another instance is posting content illegal in the local instance operation region or actively posts content
  attacking local users, a full <strong>instance block</strong> might be the last resort.</li>
  </ul>

<h3>3. Communication before Actions</h3>
<p>If any of the above behaviors start to surface from a given instance, the moderation of the local instance will attempt
to inform the moderation/administration of that instance and resolve the problems coming from there. Only if no communication succeeded or no results that resolved issues came forth, actions will be taken towards that instance.</p>

<h3>4. We won't recommend blocks</h3>
<p>Blocklists and block recommendations as an information source are taken for granted but lack accurate verification. Instead of using these options to publicly announce instances of questionable intent, we will inform local users of such, with the reminder of possible personal options for hiding content that might impact their experience of the network.</p>

<h3>5. No Guilt-by-Association/Federation</h3>
<p>If we have applied any action against Instance X, an Instance Y that federates with them will not get the same judgement applied to them. A natural connection, as by the networks design, does not imply shared interests and thus doesn't warrant the same moderation methods.</p>

<p>Regardless of all given points above, in special cases the instance moderation/administration might still opt to block
an instance, while this is tried to be avoided at most.</p>

<p>(This Covenant is based on <a href="https://github.com/pixeldesu/fediverse-friendly-moderation-covenant">The Fediverse-Friendly Moderation Covenant</a> and is licensed under CC0)</p>
