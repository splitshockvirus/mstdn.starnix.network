<h2>Terms</h2>

<strong>***By continuing to use this site, you understand that all service is subject to removal or suspension without prior notice at the administrator's discretion. Content and Actions outlined in the Code of Conduct are prohibited.***</strong>

<h3>Introduction</h3>

<p>These Website Standard Terms and Conditions written on this webpage shall manage your use of our website, Starnix accessible at mstdn.starnix.network.</p>

<p>These Terms will be applied fully and affect to your use of this Website. By using this Website, you agreed to accept all terms and conditions written in here. You must not use this Website if you disagree with any of these Website Standard Terms and Conditions. These Terms and Conditions have been generated with the help of the Terms And Conditiions Sample and the Privacy Policy Generator.</p>

<p>Children younger than 13 years old are not allowed to use this Website.</p>

<h3>Intellectual Property Rights</h3>

<p>Other than the content you own, under these Terms, Starnix and/or its licensors own all the intellectual property rights and materials contained in this Website.</p>

<p>You are a granted limited license only for purposes of viewing the material contained on this Website.</p>

<h3>Restrictions</h3>

<p>You are specifically restricted from all of the following:<p>

<ul>
<li>publishing any Website material in any other media;</li>
<li>selling, sublicensing and/or otherwise commercializing any Website material;</li>
<li>publicly performing and/or showing any Website material;</li>
<li>using this Website in any way that is or may be damaging to this Website;</li>
<li>using this Website in any way that impacts user access to this Website;</li>
<li>using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;</li>
<li>engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website;</li>
<li>using this Website to engage in any advertising or marketing.</li>
</ul>
<p>Certain areas of this Website are restricted from being access by you and Starnix may further restrict access by you to any areas of this Website, at any time, in absolute discretion. Any user ID and password you may have for this Website are confidential and you must maintain confidentiality as well.</p>

<h3>Your Content</h3>

<p>In these Website Standard Terms and Conditions, "Your Content" shall mean any audio, video text, images or other material you choose to display on this Website. By displaying Your Content, you grant Starnix a non-exclusive, worldwide irrevocable, sub licensable license to use, reproduce, adapt, publish, translate and distribute it in any and all media.</p>

<p>Your Content must be your own and must not be invading any third-party’s rights. Starnix reserves the right to remove any of Your Content from this Website at any time without notice.</p>

<h3>No warranties</h3>

<p>This Website is provided "as is," with all faults, and Starnix expresses no representations or warranties, of any kind related to this Website or the materials contained on this Website. Also, nothing contained on this Website shall be interpreted as advising you.</p>

<h3>Limitation of liability</h3>

<p>In no event shall Starnix, nor any of its officers, be held liable for anything arising out of or in any way connected with your use of this Website whether such liability is under contract. Starnix, including its officers, shall not be held liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.</p>

<h3>Indemnification</h3>

<p>You hereby indemnify to the fullest extent Starnix from and against any and/or all liabilities, costs, demands, causes of action, damages and expenses arising in any way related to your breach of any of the provisions of these Terms.</p>

<h3>Severability</h3>

<p>If any provision of these Terms is found to be invalid under any applicable law, such provisions shall be deleted without affecting the remaining provisions herein.</p>

<h3>Variation of Terms</h3>

<p>Starnix is permitted to revise these Terms at any time as it sees fit, and by using this Website you are expected to review these Terms on a regular basis.</p>

<h3>Assignment</h3>

<p>Starnix is allowed to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification. However, you are not allowed to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.</p>

<h3>Entire Agreement</h3>

<p>These Terms constitute the entire agreement between Starnix and you in relation to your use of this Website, and supersede all prior agreements and understandings.</p>

<h3>Governing Law & Jurisdiction</h3>

<p>These Terms will be governed by and interpreted in accordance with the laws of the State of Virginia, and you submit to the non-exclusive jurisdiction of the state and federal courts located in Virginia for the resolution of any disputes.</p>

<h2>Privacy Policy</h2>
<h3>What information do we collect?</h3>
<p>We collect information from you when you register on our site and gather data when you participate in the instance by reading, writing, and evaluating the content shared here.<p>

<p>You may visit our site without registering but to fully interact with this instance you will need to register and create an account. When registering on our site, you will be asked to enter your name and e-mail address. Your e-mail address will be verified by an email containing a unique link. If that link is visited, we know that you control the e-mail address.</p>

<p>When you are registered and create a post we record the IP address that the post originated from. We also may retain server logs which include the IP address of every request to our server.</p>

<h3>What do we use your information for?</h3>
<p>Any of the information we collect from you may be used in one of the following ways:</p>

<ul>
<li>To personalize your experience — your information helps us to better respond to your individual needs.
To improve our site — we continually strive to improve our site offerings based on the information and feedback we receive from you.</li>
<li>To send periodic emails — The email address you provide may be used to send you information, requested notifications, responses to inquiries, and/or other requests or questions. You can manage how your email address is used in your account's Settings area of the site.</li>
</ul>
<h3>How do we protect your information?</h3>
<p>We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information.</p>

<!---<h3>What is your data retention policy?</h3>
<p>We will make a good faith effort to:</p>

<ul>
<li>Retain server logs containing the IP address of all requests to this server no more than 90 days.</li>
<li>Retain the IP addresses associated with registered users and their posts no more than 5 years.</li>
</ul>--->
<h3>Do we use cookies?</h3>
<p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow). These cookies enable the site to recognize your browser and, if you have a registered account, associate it with your registered account.</p>

<p>We use cookies to understand and save your preferences for future visits and compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>

<h3>Do we disclose any information to outside parties?</h3>
<p>We do not sell, trade, or otherwise transfer your information (personally identifiable or not) to outside parties. We also will not send this information to trusted third parties who assist us in operating our site, even if those parties agree to keep this information confidential. We will only release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. Non-personally identifiable visitor information is not provided to other parties for marketing, advertising, or other uses.</p>

<p>The use of manual or automated systems or software to extract and collect data from this website for any purpose (‘scraping’) is prohibited without the express written consent of both the site administrators and the owner of the content which is generally, but not limited to, the creator of the content.</p>

<h3>Third party links</h3>
<p>We do not include or offer third party products or services on our site but cannot guarantee that our users (and federated users) will not make posts containing such matter. In this case, these third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites. Our Code of Conduct does not permit excessive advertising ('spam') and users who identify such posts are encourage to report them to the site moderators.</p>

<h3>Children's Online Privacy Protection Act Compliance</h3>
<p>Our site, products and services are all directed to people who are at least 13 years old. If this server is in the USA, and you are under the age of 13, then per the requirements of COPPA (<a href="https://en.wikipedia.org/wiki/Children%27s_Online_Privacy_Protection_Act">Children's Online Privacy Protection Act</a>) please do not use this site.</p>

<h3>Online Privacy Policy Only</h3>
<p>This online privacy policy applies only to information collected through our site and not to information collected offline.</p>

<h3>Your Consent</h3>
<p>By using our site, you consent to our web site privacy policy.</p>

<h3>Changes to our Privacy Policy</h3>
<p>If we decide to change our privacy policy, we will post those changes on this page.</p>

<p>This document is CC-BY-SA. It was last updated January 13, 2021.</p>

<p>Originally adapted from the <a href="https://github.com/discourse/discourse">Discourse privacy policy</a>.</p>
