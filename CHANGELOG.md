# Changelog

All notable changes to mstdn.starnix.network will be documented in this file.

## [3.4.6] - 2022-02-13
### Initial launch of mstdn.starnix.network